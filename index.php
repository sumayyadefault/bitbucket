<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><title>
        UCC Survey</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="css/styles.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- JQuery UI Css -->
    <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!--<script language="Javascript" src="js/jquery.js"></script>-->
    <script type="text/JavaScript" src='js/state.js'></script>

    <script>

        $(document).ready(function(){
              $("#txtName").prop('required',true);
              $("#txtAddress").prop('required',true);
               $("select").prop('required',true);

        });
       // })(jQuery);

    </script>
    <style>
        .caption-full > p {
            margin: 0 0 5px !important;
        }
        .caption-full {
            /*padding-left: 10%;
            padding-right: 10%;*/
            font-family: consolas !important;
        }
        .txtwidth {
            width: 300px;
        }
    </style>

</head>
<body>
<div class="container-fluid Background-spacing">

    <div class="row">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                   <tr>
                       <td colspan="2" style="background-color: #337ab7;text-align: center;   vertical-align: middle;">
                           <span style="color:White;font-size:30px;font-weight:bold;">Reply to Survey By Law Commission Of India on Uniform Civil Code</span>
                       </td>
                   </tr>



                   <tr>
                       <td class="control-label" colspan="2">
                           <div style="color: #336699; font-weight: bold;padding-left:10px">
                               Save Muslim Personal Law, Send reply to 16 questions given by Dr. B. S Chauhan, Chairman, Law Commission of India in 1 minute by following below two steps.
                           <br />
                               <br />
                               1. Fill below form and click on "Go To Survey By Law Commission Of India"
                           <br />
                               <br />
                               2. In Second page , 16 questions will be displayed. Click on Submit (answers are already selected by Default)
                           <br />
                               <br />

                               Email with all your answers will be sent to Law Commission of India
                               <br />
                               <br />

                           </div>
                       </td>
                   </tr>
               </table>
               <br />

<form method="POST" action="survey.php" id="ajax-signup-form" >
            <div class="col-xs-12 col-md-offset-4 col-md-3 col-md-offset-5">
    <div class="form-group">
    <label class="control-label">Name:</label>
    <input name="txtName" type="text" id="txtName" class="form-control txtwidth" placeholder="Name" />
        </div>
    <div class="form-group">
        <label class="control-label">Mobile No:</label>
        <input name="txtMobile" type="text" id="txtMobile" class="form-control txtwidth" placeholder="10 digit mobile no." />
    </div>
    <div class="form-group">
        <label class="control-label">Email:</label>
        <input name="txtEmail" type="text" id="txtEmail" class="form-control txtwidth" placeholder="Email" />
    </div>
    <div class="form-group">
        <label class="control-label">State: *</label>
        <select name="ddlState" id="ddlState" onchange='selct_district(this.value)' class="form-control txtwidth">


        </select>
        &nbsp;
    </div>

    <div class="form-group">
        <label class="control-label">District: *</label>
        <select name="ddlDistrict" id="ddlDistrict" class="form-control txtwidth">

        </select>
        &nbsp;

    </div>

 <div class="form-group">
        <label class="control-label">Address:</label>
        <input name="txtAddress" type="text" id="txtAddress" class="form-control txtwidth" placeholder="Address" />
    </div>

    <input id="signup" name="signup" type="submit" class="btn btn-primary" value="Go to Survey By Law Commission of India"/>

            </div>
</form>
</div>
    </div>
</body>
</html>